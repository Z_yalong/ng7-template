import {
  trigger, // 动画封装触发，外部的触发器
  state, // 转场状态控制
  style, // 用来书写基本的样式
  transition, // 用来实现css3的 transition
  animate, // 用来实现css3的animations
  keyframes // 用来实现css3 keyframes的
} from '@angular/animations';


export const fadeIn = trigger('fadeIn', [
  state('void', style({ height: 0, opacity: 0 })),
  state('*', style({ height: '*', opacity: 1 })),
  transition('void => *', [ // 进场动画
    animate(200)
  ]),
  transition('* => void', [
    animate(200)
  ]),
]);
// export const fadeIn = trigger('fadeIn', [
//   state('in', style({ display: 'none' })), // 默认元素不展开
//   transition('void => *', [ // 进场动画
//     animate(200, keyframes([
//       style({ height: 0, opacity: 0, offset: 0 }), // 元素高度0，元素隐藏(透明度为0)，动画帧在0%
//       style({ height: '*', opacity: 1, offset: 1 }) // 200ms后高度自适应展开，元素展开(透明度为1)，动画帧在100%
//     ]))
//   ]),
//   transition('* => void', [
//     animate(200, keyframes([
//       style({ height: '*', opacity: 1, offset: 0 }), // 与之对应，让元素从显示到隐藏一个过渡
//       style({ height: 0, opacity: 0, offset: 1 })
//     ]))
//   ]),
// ]);

trigger('slideDialog', [
      state('void', style({ transform: 'translate3d(0, -215%, 0) scale(1)', opacity: 0 })),
      state('enter', style({ transform: 'translate3d(0, 0, 0) scale(1)', opacity: 1 })),
      state('exit', style({ transform: 'translate3d(0, -25%, 0)', opacity: 0 })),
      transition('* => *', animate('400ms cubic-bezier(0.25, 0.8, 0.25, 1)')),
    ]);

export const moveIn = trigger('moveIn', [
  state('center', style({ transform: 'translate3d(0, 0, 0) scale(1)', opacity: 1  })), // 默认元素不展开
  state('right', style({ transform: 'translate3d(100%, 0, 0) scale(1)', opacity: 1  })), // 默认元素不展开
  transition('right => center', [ // 进场动画
    animate(200)
  ]),
  transition('* => void', [
    animate(200)
  ]),
]);

export const fadeUp = trigger('fadeUp', [
  state('*', style({ transform: 'translate3d(0, 0, 0) scale(1)', opacity: 1  })), // 默认元素不展开
  state('void', style({ transform: 'translate3d(25%, 0, 0) scale(1)', opacity: 0  })), // 默认元素不展开
  transition('void => *', [ // 进场动画
    animate(200)
  ]),
  transition('* => void', [
    animate(200)
  ]),
]);

export const navantimate = trigger('navantimate', [
  state('show', style({  left: '0px' })), // 默认元素不展开
  state('hide', style({ left: '-200px' })), // 默认元素不展开
  transition('show => hide', [ // 进场动画
    animate(300)
  ]),
  transition('hide => show', [
    animate(300)
  ]),
]);
export const contentantimate = trigger('contentantimate', [
  state('show', style({ 'margin-left': '200px' })), // 默认元素不展开
  state('hide', style({ 'margin-left': '0px' })), // 默认元素不展开
  transition('show => hide', [ // 进场动画
    animate(300)
  ]),
  transition('hide => show', [
    animate(300)
  ]),
]);