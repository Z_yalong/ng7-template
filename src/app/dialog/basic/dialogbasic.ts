import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AlertComponent } from "./alert/alert.component";
import { ConfirmComponent } from "./confirm/confirm.component";
import { ErrorComponent } from "./error/error.component";
import { InfoComponent } from "./info/info.component";
import { ModalBodyDirective } from "../../directive/modal-body.directive";
import { DialogXWService } from "../../service/dialog.service";
import {MatDialogModule} from '@angular/material';

@NgModule({
    declarations: [
        AlertComponent,
        ConfirmComponent,
        ErrorComponent,
        InfoComponent,
        ModalBodyDirective
    ],
    providers: [
        DialogXWService
    ],
    imports: [
        CommonModule,
        MatDialogModule
    ],
    exports: [  
        ModalBodyDirective
    ],
    entryComponents: [
        AlertComponent,
        ConfirmComponent,
        ErrorComponent,
        InfoComponent
    ]
})
export class DialogBasic {    
}
