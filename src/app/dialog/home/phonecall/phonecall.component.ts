import { AppconfigService } from './../../../service/appconfig.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DialogXWService } from "../../../service/dialog.service";
import { HttpService } from "../../../service/http.service";

@Component({
  selector: 'app-phonecall',
  templateUrl: './phonecall.component.html',
  styleUrls: ['./phonecall.component.css']
})
export class PhonecallComponent implements OnInit {

  title: string;
  userName: string;

  mobile: string;

  resumeId: number;

  modelForm: FormGroup;

  agent: string ;

  constructor(private ds: DialogXWService,
    public dialogRef: MatDialogRef<PhonecallComponent>, @Inject(MAT_DIALOG_DATA) private callerData: any,
    private http: HttpService,
    public appconfieService: AppconfigService,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.userName = this.callerData.name;
    this.mobile = this.callerData.mobile;
    this.title = this.userName;
    this.resumeId = this.callerData.resumeId;

    this.modelForm = this.formBuilder.group({
      agent: [this.appconfieService.agent, [Validators.required]], // , CustomValidators.validatePhone
    });

  }

  cancel() {
    this.dialogRef.close();
  }


  Call() {
    const url = "/api/Holly/Call?resumeId=" + this.resumeId + "&agent=" + this.appconfieService.agent;
    console.log(url);
    this.http.get(url, null).then(res => {
      this.ds.Info("正在呼叫...");
      this.dialogRef.close(true);
     });
  }
}
