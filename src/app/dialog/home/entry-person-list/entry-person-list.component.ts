import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DialogXWService } from "../../../service/dialog.service";
import { ApiService } from "../../../../environments/environment";
import { LazyLoadEvent } from "primeng/components/common/lazyloadevent";

@Component({
  selector: 'app-entry-person-list',
  templateUrl: './entry-person-list.component.html',
  styleUrls: ['./entry-person-list.component.css']
})
export class EntryPersonListComponent {

  option: any = { }; // 查询参数
  items=[]; // 查询结果
  totalCount: number; //总数
  constructor(public dialogRef: MatDialogRef<EntryPersonListComponent>, @Inject(MAT_DIALOG_DATA) public callerData: any,
  private ds:DialogXWService,private api: ApiService) {
      this.option.areaId = this.callerData.id;
  }


  GetList() {
    this.api.EntryPower.GetUsers(this.option).then(result => {
      this.items = result.list;
      this.totalCount = result.totalCount;
    });
  }

  selectedItem() {}

  async DelPerson(item: any) {
    const confirm = await this.ds.Confirm("你确定要删除吗?");
    if (!confirm) return;
    await this.api.EntryPower.DeleteUsers(item.id);
    this.GetList();
  }

  loadItemsLazy(event: LazyLoadEvent) {
    this.option.pageSize = event.rows;
    this.option.pageIndex = event.first / event.rows + 1;
    this.GetList();
   }

  cancel() {
    this.dialogRef.close();
  }

}
