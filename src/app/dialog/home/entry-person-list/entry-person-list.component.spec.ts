import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryPersonListComponent } from './entry-person-list.component';

describe('EntryPersonListComponent', () => {
  let component: EntryPersonListComponent;
  let fixture: ComponentFixture<EntryPersonListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntryPersonListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryPersonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
