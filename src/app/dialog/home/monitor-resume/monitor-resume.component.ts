import { Component, OnInit, Inject } from '@angular/core';
import { DomHelperService } from "../../../service/dom-helper.service";
import { ApiService } from "../../../../environments/environment";
import { LazyLoadEvent } from "primeng/components/common/lazyloadevent";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: 'app-monitor-resume',
  templateUrl: './monitor-resume.component.html',
  styleUrls: ['./monitor-resume.component.css']
})
export class MonitorResumeComponent  {
  title: string;
  items: any ;
  selectedItem: any;
  totalRecords: number;

  constructor(public dialogRef: MatDialogRef<MonitorResumeComponent>, @Inject(MAT_DIALOG_DATA) private option: any,
  public com: DomHelperService,
   private api: ApiService ) {
   }


  cancel() {
    this.dialogRef.close()
  }

  loadItemsLazy(event: LazyLoadEvent) {
    this.option.pageSize = event.rows;
    this.option.pageIndex = event.first / event.rows + 1;
    this.GetData();
  }

  GetData() {
    this.api.Monitor.GetResumeList(this.option).then(result => {
      if ( result ) {
        this.items = result.list;
        this.totalRecords = result.totalCount;
      }
    });
  }
}
