import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ApiService } from "../../../../environments/environment";


@Component({
  selector: 'app-monitor-email',
  templateUrl: './monitor-email.component.html',
  styleUrls: ['./monitor-email.component.css']
})
export class MonitorEmailComponent  {

  items: any;
  selectedItem: any;
  totalRecords: number;

  constructor(public dialogRef: MatDialogRef<MonitorEmailComponent>, @Inject(MAT_DIALOG_DATA) private option: any,
    private api: ApiService) {
  }


  loadItemsLazy(event: LazyLoadEvent) {
    this.option.pageSize = event.rows;
    this.option.pageIndex = event.first / event.rows + 1;
    this.GetData();
  }

  GetData(): void {
    this.api.Monitor.GetSendMailList(this.option).then(result => {
      this.items = result.list;
      this.totalRecords = result.totalCount;
    });
  }

  cancel() {
    this.dialogRef.close()
  }


}
