import { Component, OnInit, Input, ViewChild, ElementRef, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ApiService } from "../../../../environments/environment";
import { DialogXWService } from "../../../service/dialog.service";
import CustomValidators from '../../../demo/forms/CustomValidators';

@Component({
  selector: 'app-black-item',
  templateUrl: './black-item.component.html',
  styleUrls: ['./black-item.component.css']
})
export class BlackItemComponent implements OnInit {

  title: string;

  modelForm: FormGroup;

  model: any = {};
  constructor(public dialogRef: MatDialogRef<BlackItemComponent>,@Inject(MAT_DIALOG_DATA) private callerData: any,private ds: DialogXWService,
    private formBuilder: FormBuilder, private api: ApiService) {
  }

  cancel() {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.title = this.callerData.title;

    this.modelForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(20), CustomValidators.validUserName]],
      mobile: ['', [Validators.required, CustomValidators.validatePhone]],
      cardId: ['', [CustomValidators.validateIdCard]],
      remark: ['', [Validators.required]]
    });

    if (this.callerData.id) {
      // this.http.get("/api/Blacklist/GetOne/" + this.callerData.id).then(result => {
      // this.modelForm.setValue({ name: result.name, mobile: result.mobile, cardId: result.cardId, remark: result.remark });
      // });
      this.modelForm.setValue(this.callerData.data);
    }
  }


  async save() {
    const data: any = {};

    Object.assign(data, this.modelForm.value);
    data.name = data.name.trim();
    data.remark = data.remark.trim();
    //let url = '/api/Blacklist/Add';
    if (this.callerData.id) {
      data.id = this.callerData.id;
      // url = '/api/Blacklist/Edit';
      await this.api.Black.Edit(data);
    }
    else
      await this.api.Black.Add(data);

    this.ds.Info("保存成功！");
    this.dialogRef.close(true);
  }


}
