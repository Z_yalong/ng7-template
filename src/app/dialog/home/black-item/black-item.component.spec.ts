import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlackItemComponent } from './black-item.component';

describe('BlackItemComponent', () => {
  let component: BlackItemComponent;
  let fixture: ComponentFixture<BlackItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlackItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlackItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
