import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { Component, OnInit, Inject } from '@angular/core';
import { ApiService } from "../../../../environments/environment";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: 'app-monitor-mobile',
  templateUrl: './monitor-mobile.component.html',
  styleUrls: ['./monitor-mobile.component.css']
})
export class MonitorMobileComponent  {

  items: any;
  selectedItem: any;
  totalRecords: number;

  constructor(public dialogRef: MatDialogRef<MonitorMobileComponent>, @Inject(MAT_DIALOG_DATA) private option: any, private api: ApiService) {
   }

  cancel() {
    this.dialogRef.close()
  }

  loadItemsLazy(event: LazyLoadEvent) {
    this.option.pageSize = event.rows;
    this.option.pageIndex = event.first / event.rows + 1;    
    this.GetData();
  }

  GetData(): void {
    this.api.Monitor.GetMobileList(this.option).then(result => {
      if (result) {
        this.items = result.list;
        this.totalRecords = result.totalCount;
      }
    });
  }

}
