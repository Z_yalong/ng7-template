import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntryPermissionComponent } from './entry-permission.component';

describe('EntryPermissionComponent', () => {
  let component: EntryPermissionComponent;
  let fixture: ComponentFixture<EntryPermissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntryPermissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryPermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
