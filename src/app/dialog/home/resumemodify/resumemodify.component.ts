import { style } from '@angular/animations';
import { Component, OnInit, Input, ViewChild, ElementRef, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { RequestOptionsArgs, Headers } from "@angular/http";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { DialogXWService } from "../../../service/dialog.service";
import { ApiService } from "../../../../environments/environment";
import { PositionService } from "../../../service/position.service";
import CustomValidators from "../../../demo/forms/CustomValidators";

@Component({
  selector: 'app-resumemodify',
  templateUrl: './resumemodify.component.html',
  styleUrls: ['./resumemodify.component.css']
})
export class ResumemodifyComponent  implements OnInit {

  title: string;

  modelForm: FormGroup;

  model: any = {};

  @ViewChild('inputfile') inputfile: ElementRef;

  filename = '选择文件';
  file: File;

  style: any ;
  postionlist;
  constructor(private formBuilder: FormBuilder,
    private ds: DialogXWService,
    public dialogRef: MatDialogRef<ResumemodifyComponent>, @Inject(MAT_DIALOG_DATA) private callerData: any,
    private api: ApiService, public postionservice: PositionService) {
      postionservice.positionList.then(list=>{
        this.postionlist=list;
      })
  }


  ngOnInit() {
    this.style =  { 'width': '240px' };
    this.title = this.callerData.title;

    this.modelForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(20), CustomValidators.validUserName]],
      mobile: ['', [Validators.required, CustomValidators.validatePhone]],
      eMail: ['', [Validators.required, Validators.maxLength(50), CustomValidators.validateEmail]],
      postId: [0, [Validators.required]]
    });
    // tslint:disable-next-line:curly
    if (this.callerData.id) {
      this.modelForm.setValue(this.callerData.data);
      //this.http.get("/api/Resumes/GetOne/" + this.callerData.id).then(result => {
      //  this.modelForm.setValue({name: result.name, mobile: result.mobile, eMail: result.eMail, postId: result.postId});
      //});
      // let data=this.callerData.data;
    }
  }

  cancel() {
    this.dialogRef.close();
  }

  async save() {
    // let formData: FormData = new FormData();
    // formData.append('uploadFile', this.file, this.file.name);
    // let headers = new Headers({
    //     "Accept": "application/json"
    // });

    if (!this.callerData.id) {
      if (this.modelForm.value.postId <= 0) {
        this.ds.Alert("请选择岗位！");
        return;
      }

      if (!this.file) {
        this.ds.Alert("请上传简历附件！");
        return;
      }
    }

    if(this.file&&this.file.size/1024/1024>100){
      this.ds.Alert("简历附件大小不超过100MB！");
      return;
    }

    let formData: FormData = new FormData();

    if ( this.file ) {
      formData.append('resumeFile', this.file, this.file.name);
    }
    formData.append('name', this.modelForm.value.name.trim());
    formData.append('mobile', this.modelForm.value.mobile);
    formData.append('email', this.modelForm.value.eMail);
    formData.append('postId', this.modelForm.value.postId);

    // let headers = new Headers({
    //   'Accept': 'application/json'
    // });
    // ,<RequestOptionsArgs>{headers:headers}
    //let url = '/api/Resumes/Add';
    if (this.callerData.id) {
      formData.append('id', this.callerData.id);
      //url = '/api/Resumes/Edit';
      await this.api.Resumes.Edit(formData);
    }
    else
      await this.api.Resumes.Add(formData);
    //this.http.post(url, formData, <RequestOptionsArgs>{headers: headers}).then(result => {
    //this.ds.Info("保存成功！");
    this.dialogRef.close(true);
    //});

  }

  onFileChanged(fileList: FileList) {
    console.log(fileList[0].size);
    if (fileList.length > 0) {
      this.file = fileList[0];
      this.filename = this.file.name;
    }
  }

  chooseFile() {
    this.inputfile.nativeElement.click();
  }
}
