import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ApiService } from "../../../../environments/environment";

@Component({
  selector: 'app-monitor-face',
  templateUrl: './monitor-face.component.html',
  styleUrls: ['./monitor-face.component.css']
})
export class MonitorFaceComponent  {

  items: any;
  selectedItem: any;
  totalRecords: number;

  constructor(public dialogRef: MatDialogRef<MonitorFaceComponent>, @Inject(MAT_DIALOG_DATA) private option: any, private api: ApiService) {
  }


  loadItemsLazy(event: LazyLoadEvent) {
    this.option.pageSize = event.rows;
    this.option.pageIndex = event.first / event.rows + 1;
    this.GetData();
  }

  GetData(): void {
    this.api.Monitor.GetInterviewList(this.option).then(result => {
      console.log(result);
      if ( result ) {
        this.items = result.list;
        this.totalRecords = result.totalCount;
      }
    });
  }

  cancel() {
    this.dialogRef.close();
  }

}
