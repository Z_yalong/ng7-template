import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { OrganizationService } from "../../../service/organization.service";
import { DialogXWService } from "../../../service/dialog.service";
import { ApiService } from "../../../../environments/environment";
import { RequestOptionsArgs } from "@angular/http";
import { TreeNode } from "primeng/components/common/treenode";

@Component({
  selector: 'app-recruitpermission',
  templateUrl: './recruitpermission.component.html',
  styleUrls:  ['./recruitpermission.component.css']
})
export class RecruitpermissionComponent implements OnInit {
  departments : TreeNode[] ;
  selectingdepartments : TreeNode[] ;

  selecteddepartments:{ id: number, name: string, path: string }[] = [] ;//最终选项
  selectingdepartments2 : { id: number, name: string, path: string }[];

  persondepartments : TreeNode[] ;
  selectingdepartment : TreeNode ;  
  persons: any[];
  selectingpersons: any[];

  selecteddepersons = [] ;//最终选项
  selectingpersons2 : any[];

  cities=[];//城市
  selectedcities1=[];//已选择的城市1
  selectedcities2=[];//已选择的城市2
  
  departname:string;
  personname:string;

  departmentstyle1:any;
  departmentstyle2:any;
  constructor( private ds: DialogXWService,public organization: OrganizationService
    ,private api: ApiService, public dialogRef: MatDialogRef<RecruitpermissionComponent>) {
      this.departmentstyle1={'height':'415px','margin-top':'5px','width':'415px'};
      this.departmentstyle2={'height':'200px','margin-top':'5px','width':'415px'};
  }

  ngOnInit() {
    this.organization.GetTreeData().then(items=>{
      this.departments=items;
    });
    this.organization.GetTreeData().then(items=>{
      if(items.length) this.selectingdepartment=items[0];
      this.persondepartments=items;
    });
    this.cities=[{id:'001',name:'全国'},{id:'002',name:'上海'},{id:2,name:'苏州'},{id:3,name:'杭州'},{id:4,name:'北京'}]
  }

  SerchDepart(departname:string,type:number){
    this.organization.GetTreeData(departname).then(items => {
      if (type == 1)
        this.departments = items;
      else if (type == 2)
        this.persondepartments = items;
    });
  }

  SerchPerson(name:string){
    let option: RequestOptionsArgs = { params: {} };
    if (name) option.params['name'] = name;
    let departmentid = 0;
    if (this.selectingdepartment) departmentid = this.selectingdepartment.data.id;
    this.api.Info.GetEmployeeList(departmentid,option).then(persons=>{
      this.persons=persons;
    })
  }

  GetPerson(event){
    this.api.Info.GetEmployeeList(this.selectingdepartment.data.id).then(persons=>{
      this.persons=persons;
    })
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    if(!this.selecteddepartments.length&&!this.selecteddepersons.length){
      this.ds.Alert("部门或人员必选一个!");
      return;
    } 
    if(this.selecteddepartments.length&&!this.selectedcities1.length){
      this.ds.Alert("必须选择部门对应的城市!");
      return;
    }
    if(this.selecteddepersons.length&&!this.selectedcities2.length){
      this.ds.Alert("必须选择人员对应的城市!");
      return;
    }
    
    let departments = null;
    if (this.selecteddepartments.length && this.selectedcities1.length)
      departments = { powers: this.selecteddepartments, cityIds: this.selectedcities1.map(item=>item.id) };
    let persons = null;
    if (this.selecteddepersons.length && this.selectedcities2.length) {
      persons = { powers: this.selecteddepersons, cityIds: this.selectedcities2.map(item=>item.id) };
    }
    let data = {
      department: departments
      , person: persons
    };

    // console.log(data);
    // console.log(this.selectedcities1);
    this.api.Recruit.Add(data).then(result=>{
      this.ds.Info("保存成功！");
      this.dialogRef.close(true);
    });
  }

  AddDepartment(){
    if (this.selectingdepartments && this.selectingdepartments.length) {
      this.selectingdepartments.forEach((item, index) => {
        if (item.data.id != 0)
          this.selecteddepartments.push(item.data);
      });
      let existitem:any={};
      this.selecteddepartments = this.selecteddepartments.filter((item) => {
        if (existitem[item.path])
          return false;
        else {
          existitem[item.path] = true;
          return true;
        }
      });
    }
  }

  DelDepartment(){
    if(this.selectingdepartments2 && this.selectingdepartments2.length){
      let curselected = this.selectingdepartments2;
      this.selecteddepartments = this.selecteddepartments.filter(item=>!curselected.find(s=>s.path==item.path))
    }
  }

  AddPerson(){
    if (this.selectingpersons && this.selectingpersons.length) {
      this.selectingpersons.forEach((item, index) => {
        this.selecteddepersons.push(item);
      });
      let existitem:any={};
      this.selecteddepersons = this.selecteddepersons.filter((item) => {
        if (existitem[item.id])
          return false;
        else {
          existitem[item.id] = true;
          return true;
        }
      });
    }
  }

  DelPerson(){
    if(this.selectingpersons2 && this.selectingpersons2.length){
      let curselected = this.selectingpersons2;
      this.selecteddepersons = this.selecteddepersons.filter(item=>!curselected.find(s=>s.id==item.id))
    }
  }

  checkchange(selectedcities,item,e){
    if(e){
      if(item.id==1){
        selectedcities.length=0;
        selectedcities.push(item);
      }
      else{
        selectedcities.forEach((item,index)=>{
          if(item.id==1){
            selectedcities.splice(index,1);
          }
          console.log(index);
        })
      }
    }
    //   console.log(selectedcities.length);
    //   console.log(selectedcities);
    // console.log(e);
  }
}
