import { Injectable } from '@angular/core';
import {
    Resolve,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
} from '@angular/router';
import { TabService } from "./tabservice.service";
import {Observable} from 'rxjs';

@Injectable()
export class StockResolveService implements Resolve<any>{
    constructor(private tab: TabService) {

    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any | Observable<any> | Promise<any> {
        this.tab.getmenu.then(()=>{
            this.tab.tabmanager.emit(route.routeConfig.path);
        })
        return null;
    }
}
