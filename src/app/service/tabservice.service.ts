import { Injectable, EventEmitter } from "@angular/core";
import { treemenunode } from "../class/treemenunode";
import { AppconfigService } from "./appconfig.service";

@Injectable()
export class TabService {
    tabmanager: EventEmitter<string>;
    getmenu: Promise<treemenunode[]>;//初始化菜单

    selectindex: number = 0;//当前活动tab窗口的index
    tabs = [];//tab列表
    treeData: treemenunode[] = [];
    constructor(private config: AppconfigService) {
        this.tabmanager =new EventEmitter<string>();

        let menu1 = new treemenunode('1 招聘管理', 'fa fa-address-card', null, null, []);
        let menu1_1 = new treemenunode('1.0 简历管理', null, 'resume', "简历管理");
        let menu1_2 = new treemenunode('1.1 黑名单管理', null, 'blacklist', "黑名单管理");
       
        let menu2 = new treemenunode('2 监控管理', 'fa fa-window-restore', null, null, []);
        let menu2_1 = new treemenunode('2.0 指标日报表', 'fa fa-window-restore', 'monitorday', "指标日报表");
        let menu2_2 = new treemenunode('2.1 指标时间段报表', 'fa fa-window-restore', 'monitor', "指标时间段报表");
    
        let menu3 = new treemenunode('3 数据权限配置', 'fa fa-database', null, null, []);
        let menu3_1 = new treemenunode('3.0 招聘权限管理', null, 'recruit', "招聘权限管理");
        let menu3_2 = new treemenunode('3.1 入职权限管理', null, 'entry', "入职权限管理");
    
        let menu4 = new treemenunode('4 计算规则', 'fa fa-window-restore', 'rule', null);

 
        this.getmenu = config.getpermision.then(authority => {

            if (config.authority['HRMS_01_01'])
                menu1.children.push(menu1_1);
            if (config.authority['HRMS_01_02'])
                menu1.children.push(menu1_2);
            if (config.authority['HRMS_01'])
                this.treeData.push(menu1);
            menu2.children.push(menu2_1);
            menu2.children.push(menu2_2);
            if (config.authority['HRMS_02'])
                this.treeData.push(menu2);
            if (config.authority['HRMS_03_01'])
                menu3.children.push(menu3_1);
            if (config.authority['HRMS_03_02'])
                menu3.children.push(menu3_2);
            if (config.authority['HRMS_03'])
                this.treeData.push(menu3);
            return this.treeData;
        })
        
        this.tabs.push(new treemenunode('首页', 'fa fa-window-restore', 'index', "首页"));
    
        this.tabmanager.subscribe(routername=>{
          let isadd = true;
          this.tabs.forEach((item,index)=>{
            if(item.routername == routername){
              this.selectindex = index;
              isadd = false;
            }
          })
          if (isadd) {
            this.treeData.forEach(node1=>{
              if(node1.children&&node1.children.length){
                node1.children.forEach(node2=>{
                  if(node2.routername==routername)  {
                    this.tabs.push(node2);
                    this.selectindex = this.tabs.length - 1;
                  }
                })
              }
            })
          }
        });
    }
}