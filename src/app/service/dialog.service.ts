import { Injectable, TemplateRef } from '@angular/core';

import { InfoComponent } from "../dialog/basic/info/info.component";
import { AlertComponent } from "../dialog/basic/alert/alert.component";
import { ConfirmComponent } from "../dialog/basic/confirm/confirm.component";
import { ErrorComponent } from "../dialog/basic/error/error.component";
import { ComponentType } from "@angular/cdk/portal";
import { MatDialog, MatDialogConfig } from "@angular/material";



@Injectable()
export class DialogXWService {

  constructor(public dialog: MatDialog) { }

  Info(data: string) {
    return this.dialog.open(InfoComponent, <MatDialogConfig>{
      disableClose: true,
      DialogRole: 'alertdialog',//'dialog'
      data:data ,
      width: '30%',
      // height: '0',
      //position:{left:'0',right:'0',top:'0',bottom:'0',position:'absolute'}
    }).afterClosed().toPromise();
  }
  /**
       * alert重写
       */
  Alert(data: string) {
    return this.dialog.open(AlertComponent, <MatDialogConfig>{
      disableClose: true,
      DialogRole: 'alertdialog',//'dialog'
      data:data ,
      width: '30%',
      // height: '0',
      //position:{left:'0',right:'0',top:'0',bottom:'0',position:'absolute'}
    }).afterClosed().toPromise();
  }

  Confirm(data: string):Promise<boolean>{
    return this.dialog.open(ConfirmComponent, <MatDialogConfig>{
      disableClose: true,
      DialogRole: 'alertdialog',//'dialog'
      data:data ,
      width: '30%'
    }).afterClosed().toPromise();
  }

  isexsit = false;
  async Error(data: string, norepeat?: boolean) {
    if (norepeat) {
      if (this.isexsit)
        return Promise.resolve();
      else
        this.isexsit = true;
    }

    await this.dialog.open(ErrorComponent, <MatDialogConfig>{
      disableClose: true,
      DialogRole: 'alertdialog',//'dialog'
      data: data,
      width: '30%',
    }).afterClosed().toPromise();

    if (norepeat) {
      this.isexsit = false;
    }
  }

  /** 定制弹框 */
  open<T, D = any>(componentOrTemplateRef: ComponentType<T> | TemplateRef<T>, config?: MatDialogConfig<D>){
    return this.dialog.open(componentOrTemplateRef,config).afterClosed().toPromise();
  }
}
