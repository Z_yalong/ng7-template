import {Injectable} from '@angular/core';
import {ApiService} from '../../environments/environment';

@Injectable()
export class AppconfigService {

  agent: string; // 坐席号码

  counter = 0;
  //日期配置
  calendar = {
    ch: {
      firstDayOfWeek: 0,
      dayNames: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
      dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
      monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
      monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    }
  };

  getpermision: Promise<any>;
  username: string;//登入信息
  authority: any = {};//权限
  constructor(private api: ApiService) {
    this.getpermision = api.GetPermissions().then((result:any) => {
      this.username = result.fullname;
      let authorities: { fKey: string, permissions: string[] }[] = result.authorities;
      let authority = this.authority;
      authorities.forEach(item => {
        authority[item.fKey] = {};
        item.permissions.forEach(p => {
          authority[item.fKey][p] = true;
        });
      });
      return authority;
    });
  }


}
