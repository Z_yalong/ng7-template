import { Injectable } from '@angular/core';
import {Spinner, SpinnerOptions} from 'spin.js';

@Injectable()
export class BlockUiService {
  private isblock: boolean = false;
  private count = 0;
  spinner: Spinner;
  div: HTMLElement;
  constructor() {
    this.spinner = new Spinner(<SpinnerOptions>{
      lines: 13, // 花瓣数目
      length: 0, // 花瓣长度
      width: 8, // 花瓣宽度
      radius: 30, // 花瓣距中心半径
      corners: 1, // 花瓣圆滑度 (0-1)
      rotate: 0, // 花瓣旋转角度
      direction: 1, // 花瓣旋转方向 1: 顺时针, -1: 逆时针
      color: '#5882FA', // 花瓣颜色
      speed: 1, // 花瓣旋转速度
      trail: 50, // 花瓣旋转时的拖影(百分比)
      shadow: false, // 花瓣是否显示阴影
      hwaccel: false, //spinner 是否启用硬件加速及高速旋转
      className: 'spinner', // spinner css 样式名称
      zIndex: 2e9, // spinner的z轴 (默认是2000000000)
      top: '50%', // spinner 相对父容器Top定位 单位 px
      left: '50%'// spinner 相对父容器Left定位 单位 px
    });
    this.div = document.createElement("div");
    this.div.style.position = 'fixed';
    this.div.style.top = '0';
    this.div.style.right = '0';
    this.div.style.bottom = '0';
    this.div.style.left = '0';
    this.div.style.zIndex = '9999';
  }
  /**
       * 阻塞页面
       */
  blockUI() {
    this.count = this.count + 1;
    if (this.count == 1) {
      this.isblock = true;
      document.body.appendChild(this.div);
      this.spinner.spin(this.div);
    }
  }
  /**
      * 取消阻塞
      */
  UnBlockUI() {
    this.count = this.count - 1;
    if (this.count < 0) this.count = 0;
    if (this.count == 0) {
      this.isblock = false;
      this.spinner.spin();
      document.body.removeChild(this.div);
    }
  }
}
