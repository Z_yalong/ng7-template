import { Injectable } from "@angular/core"
import { ActivatedRoute, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router'
import { Observable } from "rxjs";
import { AppconfigService } from "./appconfig.service";

@Injectable()
export class Authentication implements CanActivate {

    routepermisson: any = {
        resume: 'HRMS_01_01',
        blacklist: 'HRMS_01_02',
        monitorday: 'HRMS_02_01',
        monitor: 'HRMS_02_01',
        recruit: 'HRMS_03_01',
        entry: 'HRMS_03_02'
    }
    constructor(private oldRoute: ActivatedRoute,private config: AppconfigService) {

     }
    canActivate(newRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        // console.log(this.oldRoute); //old route
        // console.log(newRoute); //new route 
        // console.log(state); //new state 
        
        return new Promise<boolean>((resolve, reject) => {
            // setTimeout(() => {
            // resolve(true);
            // }, 1000);
            this.config.getpermision.then(auth => {
                if (auth[this.routepermisson[newRoute.routeConfig.path]])
                    resolve(true);
            });
        });
    }
}
