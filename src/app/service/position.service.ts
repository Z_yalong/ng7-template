import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { error } from 'util';
import { ApiService } from "../../environments/environment";
import { SelectItem } from "primeng/components/common/selectitem";


@Injectable()
export class PositionService {

  positionList:Promise<SelectItem[]>;

  constructor(private api: ApiService) {
    this.positionList = this.api.Info.GetPositionList().then((result:any) => {
      const tmpPositionList: Array<SelectItem> = [];
      const tmpList = result.list;
      tmpPositionList.push({
        label: "请选择", value: ""
      });
      for (let index = 0; index < tmpList.length; index++) {
        const element:any = tmpList[index];
        tmpPositionList.push({
          label: element.postname,
          value: element.postid
        });
      }
      return tmpPositionList;
    });
  }
}
