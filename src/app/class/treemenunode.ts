export class treemenunode {
    name: string;
    isExpend: boolean;
    iconclass: string;
    routername: string;
    type: any;
    title: string;
    animationstate: string;
    children: treemenunode[];
    active: boolean;
    constructor(node_name: string, icon?: string, routername?: any, title?: string, children?: treemenunode[]) {
        this.name = node_name;
        if (routername)
            this.routername = routername;
        if (icon)
            this.iconclass = icon;
        if (title)
            this.title = title;
        if (children)
            this.children = children;
    }
}