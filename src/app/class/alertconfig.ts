export class AlertConfig {
  title: string;
  content: string;
}