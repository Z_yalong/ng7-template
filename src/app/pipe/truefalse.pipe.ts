import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truefalse'
})
export class TrueFalsePipe implements PipeTransform {

  transform(value: any): any {
    if (value)
      return "是";
    else 
      return "否";
  }

}