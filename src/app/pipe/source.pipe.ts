import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'source'
})
export class SourcePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value == 1)
      return "手工录入";
    else if (value == 2)
      return "拉钩网";
    return null;
  }

}