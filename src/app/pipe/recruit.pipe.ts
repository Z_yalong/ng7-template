import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'recruit'
})
export class RecruitPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value == 1)
      return "个人";
    else if (value == 2)
      return "部门";
    return null;
  }

}