import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgZorroAntdModule, NZ_I18N, zh_CN } from 'ng-zorro-antd';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';
import {BlockUiService} from './service/blockui.service';
import {AppconfigService} from './service/appconfig.service';
import {ApiService} from '../environments/environment';
import {HttpService} from './service/http.service';
import {DialogBasic} from './dialog/basic/dialogbasic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Authentication} from './service/authentication';
import {DomHelperService} from './service/dom-helper.service';
import {PositionService} from './service/position.service';
import {RouteReuseStrategy} from '@angular/router';
import {MyRouterReuseStrategy} from './service/router.snapshot';

registerLocaleData(zh);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    NgZorroAntdModule,
    DialogBasic
  ],
  providers: [
    { provide: NZ_I18N, useValue: zh_CN },
    BlockUiService,
    AppconfigService,
    ApiService,
    HttpService,
    Authentication,
    DomHelperService,
    PositionService,
    { provide: RouteReuseStrategy, useClass: MyRouterReuseStrategy }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
