import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import {RouterModule, Routes} from '@angular/router';
import {MenubarModule} from 'primeng/menubar';
import {MatButtonModule} from '@angular/material';
import {NavigationmenuComponent} from '../share/navigationmenu/navigationmenu.component';
import {NavtreeComponent} from '../share/navtree/navtree.component';
import {NavtreeitemComponent} from '../share/navtreeitem/navtreeitem.component';
import {NavheadComponent} from '../share/navhead/navhead.component';
import {TabService} from '../../service/tabservice.service';
import {StockResolveService} from '../../service/stock-resole.service';
import {Authentication} from '../../service/authentication';



const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: '',redirectTo:'index' },
      { path: 'index', loadChildren: "./indexpage/indexpage.module#IndexpageModule" ,data:{snapshot:true},resolve:{stock: StockResolveService} },
      { path: 'resume', loadChildren: "./resume/resume.module#ResumeModule" , canActivate: [Authentication] ,data:{snapshot:true},resolve:{stock: StockResolveService}},
      { path: 'blacklist', loadChildren: "./blacklist/blacklist.module#BlacklistModule" , canActivate: [Authentication] ,data:{snapshot:true},resolve:{stock: StockResolveService}},
      { path: 'monitorday', loadChildren: "./monitor-day/monitor-day.module#MonitorDayModule" , canActivate: [Authentication] ,data:{snapshot:true},resolve:{stock: StockResolveService}},
      { path: 'monitor', loadChildren: "./monitor/monitor.module#MonitorModule" , canActivate: [Authentication] ,data:{snapshot:true},resolve:{stock: StockResolveService}},
      { path: 'recruit', loadChildren: "./recruit/recruit.module#RecruitModule", canActivate: [Authentication] ,data:{snapshot:true},resolve:{stock: StockResolveService} },
      { path: 'entry', loadChildren: "./entry/entry.module#EntryModule", canActivate: [Authentication] ,data:{snapshot:true},resolve:{stock: StockResolveService} }
    ]
  }
];


@NgModule({
  declarations: [
    HomeComponent,
    NavigationmenuComponent,
    NavtreeComponent,
    NavtreeitemComponent,
    NavheadComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MenubarModule,
    MatButtonModule
  ],
  providers:[
    StockResolveService,
    TabService
  ]
})
export class HomeModule { }
