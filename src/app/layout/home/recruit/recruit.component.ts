import { Component, OnInit } from '@angular/core';
import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { ApiService } from "../../../../environments/environment";
import { OrganizationService } from "../../../service/organization.service";
import { AppconfigService } from "../../../service/appconfig.service";
import { DialogXWService } from "../../../service/dialog.service";
import { RecruitpermissionComponent } from "../../../dialog/home/recruitpermission/recruitpermission.component";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

@Component({
  selector: 'app-recruit',
  templateUrl: './recruit.component.html',
  styleUrls: ['./recruit.component.css']
})
export class RecruitComponent implements OnInit {
  option: any = { }; // 查询参数
  items: any; // 查询结果
  totalCount: number; //总数

  selectedItems = [];//已选则项

  first=0;//第几页
  authority:any={};//权限

  width38={width:'38px'};
  width60={width:'60px'};
  constructor(private api: ApiService, public dialog: MatDialog,private organization: OrganizationService
  ,private config: AppconfigService,private ds: DialogXWService) {
    config.getpermision.then(auth=>{
      this.authority = auth['HRMS_03_01'];
    })
   }

  ngOnInit() {
    //this.items.push({});
  }


  loadItemsLzay(event: LazyLoadEvent) {
    this.option.pageSize = event.rows;
    this.option.pageIndex = event.first / event.rows + 1;
    this.GetData();
   }

  Add(){
    this.dialog.open(RecruitpermissionComponent, <MatDialogConfig>{
      disableClose: true,
      width: '900px'
    }).afterClosed().subscribe((result) => {
       if(result) this.GetData();
    });
  }

  GetData() {
    this.api.Recruit.GetList(this.option).then(result => {
      this.items = result.list;
      this.totalCount = result.totalCount;
      console.log(result);
    });
  }

  async Del() {
    if (!this.selectedItems.length) {
      this.ds.Alert("请先选择要删除的选项！");
      return;
    }
    if (!await this.ds.Confirm("你确定要删除吗?")) return;
    let delids = this.selectedItems.map(item => item.id);
    await this.api.Recruit.Delte(delids);
    this.ds.Info("删除成功！");
    this.GetData();
  }
}
