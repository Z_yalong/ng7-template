import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { RecruitComponent } from "./recruit.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DialogXWService } from "../../../service/dialog.service";
import { RecruitpermissionComponent } from "../../../dialog/home/recruitpermission/recruitpermission.component";
import { RecruitPipe } from "../../../pipe/recruit.pipe";
import { OrganizationService } from "../../../service/organization.service";
import { ModalBodyDirective } from "../../../directive/modal-body.directive";
import { DialogBasic } from "../../../dialog/basic/dialogbasic";
import { DataTableModule } from "primeng/components/datatable/datatable";
import { TabViewModule } from "primeng/components/tabview/tabview";
import { TreeModule } from "primeng/components/tree/tree";
import { CheckboxModule } from "primeng/components/checkbox/checkbox";

const routes: Routes = [
  {
    path: '',
    component: RecruitComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    TabViewModule,
    TreeModule,
    CheckboxModule,
    DialogBasic
  ],
  declarations: [
    RecruitComponent,
    RecruitpermissionComponent,
    RecruitPipe
  ],
  entryComponents:[
    RecruitpermissionComponent
  ],
  providers:[
    OrganizationService
  ]
})
export class RecruitModule { }
