import { Component, OnInit } from '@angular/core';
import { AppconfigService } from "../../../service/appconfig.service";
import { EntryPermissionComponent } from "../../../dialog/home/entry-permission/entry-permission.component";
import { EntryPersonListComponent } from "../../../dialog/home/entry-person-list/entry-person-list.component";
import { ApiService } from "../../../../environments/environment";
import { DialogXWService } from "../../../service/dialog.service";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";

@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css']
})
export class EntryComponent implements OnInit {

  option: any = { }; // 查询参数
  items: any; // 查询结果
  totalCount: number; //总数

  authority:any={};//权限
  constructor(private api: ApiService, public dialog: MatDialog
    ,private ds: DialogXWService,private config: AppconfigService) { 
      config.getpermision.then(auth=>{
        this.authority = auth['HRMS_03_02'];
      })
  }

  ngOnInit() {
    this.GetData();
  }
  GetData() {
    this.api.GetEntryAreaList().then(result => {
      this.items = result;
    });
  }

  loadItemsLazy(event: any) { 
    this.option.pageSize = event.rows;
    this.option.pageIndex = event.first / event.rows + 1;
    this.GetData();
  }

  async Config(item){
    const result = await this.dialog.open(EntryPermissionComponent, <MatDialogConfig>{
      disableClose: true,
      data:{ id: item.id },
      width: '900px'
    }).afterClosed().toPromise();
    if (result) {
      //this.ds.Info('成功分配人员权限！');
      this.GetData();
    }
  }

  selectedItem() { }

  

  EditPerson(item: any) {
    this.dialog.open(EntryPersonListComponent, <MatDialogConfig>{
      disableClose: true,
      data:{id: item.id,title: item.name} ,
      width: '600px'
    });
    // .afterClosed().subscribe(()=>{
    //   this.GetData();
    // });

  }
}
