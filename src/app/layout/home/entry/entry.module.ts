import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntryComponent } from "./entry.component";
import { Routes, RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DialogXWService } from "../../../service/dialog.service";
import { EntryPermissionComponent } from "../../../dialog/home/entry-permission/entry-permission.component";
import { DialogBasic } from "../../../dialog/basic/dialogbasic";
import { OrganizationService } from "../../../service/organization.service";
import { EntryPersonListComponent } from "../../../dialog/home/entry-person-list/entry-person-list.component";
import { DataTableModule } from "primeng/components/datatable/datatable";
import { TreeModule } from "primeng/components/tree/tree";

const routes: Routes = [
  {
    path: '',
    component: EntryComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    DialogBasic,
    DataTableModule,
    TreeModule
  ],
  declarations: [
    EntryComponent,
    EntryPermissionComponent,
    EntryPersonListComponent
  ],
  entryComponents:[
    EntryPermissionComponent,
    EntryPersonListComponent
  ],
  providers:[
    OrganizationService
  ]
})
export class EntryModule { }
