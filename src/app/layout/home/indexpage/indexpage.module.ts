import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, Routes, RouterModule } from "@angular/router";
import { IndexpageComponent } from "./indexpage.component";

const routes: Routes = [
  {
    path: '',
    component: IndexpageComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [IndexpageComponent]
})
export class IndexpageModule { }
