import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-indexpage',
  templateUrl: './indexpage.component.html',
  styleUrls: ['./indexpage.component.css']
})
export class IndexpageComponent implements OnInit {

  constructor(private thisroute: ActivatedRoute) {
    console.log(thisroute.data["value"])
   }

  ngOnInit() {
  }

}
