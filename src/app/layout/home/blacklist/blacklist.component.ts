
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { BlackItemComponent } from '../../../dialog/home/black-item/black-item.component';
import { AppconfigService } from "../../../service/appconfig.service";
import {  MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { ApiService } from "../../../../environments/environment";
import { DialogXWService } from "../../../service/dialog.service";

@Component({
  selector: 'app-blacklist',
  templateUrl: './blacklist.component.html',
  styleUrls: ['./blacklist.component.css']
})
export class BlacklistComponent implements OnInit {
  option: any = {}

  items: any;
  totalRecords: number;
  filename = '选择文件';
  file: File;
  @ViewChild('inputfile') inputfile: ElementRef;

  first=0;//第几页
  authority: any = {}; // 权限

  width38={width:'60px'};
  constructor(private api: ApiService, public dialog: MatDialog
    ,private ds: DialogXWService, private config: AppconfigService) {
    config.getpermision.then(auth => {
      this.authority = auth['HRMS_01_02'];
    })
  }

  ngOnInit() {
  }

  selectedItem() {

  }

  loadItemsLazy(event: any) {
    this.option.pageSize = event.rows;
    this.option.pageIndex = event.first / event.rows + 1;
    this.GetData();
  }

  async GetData() {
    if (this.option.name) {
      this.option.name = this.option.name.trim();
    }
    if (this.option.mobile) {
      this.option.mobile = this.option.mobile.trim();
    }

    const result = await this.api.Black.GetList(this.option);

    this.items = result.list;
    this.totalRecords = result.totalCount;
    // const first = this.first;
    // this.items.forEach((item, index) => {
    //   item.index = first + index + 1;
    // });
  }

  Serach(): void {
    if (this.first)
      this.first = 0;
    else
      this.GetData();
    // console.log(this.option);
  }

  chooseFile() {
    this.inputfile.nativeElement.value = '';
    this.inputfile.nativeElement.click();
  }

  onFileChanged(fileList: FileList) {
    this.file = fileList[0];
    if (this.file) {
      //
      // this.filename = this.file.name;
      // const formData = {
      // // "contentType": "string",
      // // "contentDisposition": "string",
      // // "headers": {},
      // // "length": 0,
      // "name": this.file,
      // "fileName":  this.filename
      // };

      // const headers = new Headers({
      //   'Accept': 'application/json'
      // });
      // let headers = new Headers({
      //   'Accept': 'application/json'
      // });
      let formData: FormData = new FormData();
      formData.append('excelfile', this.file, this.file.name);
      this.api.Black.ImportExcel(formData).then(result => {
        //this.http.post("/api/Blacklist/ImportExcel", this.file).then(result => {
        this.ds.Info("导入成功！");
        this.GetData();
      });
    }
  }

  Add() {
    this.dialog.open(BlackItemComponent, <MatDialogConfig>{
      data: { title: '新增' },
      disableClose: true,
      width: '600px'
    }).afterClosed().subscribe((result) => {
       if(result) this.GetData();
    });
  }


  async Modify(item: any) {
    const result:any = await this.api.Black.GetOne(item.id);
    this.dialog.open(BlackItemComponent, <MatDialogConfig>{
      data: { title: '修改', id: item.id, data: { name: result.name, mobile: result.mobile, cardId: result.cardId, remark: result.remark } },
      disableClose: true,
      width: '600px'
    }).afterClosed().subscribe((result) => {
      if (result) this.GetData();
    });
  }

  async Del(item: any) {
    const result = await this.ds.Confirm('确认将[' + item.name + ']从黑名单中删除?');
    if (!result) return;
    await this.api.Black.Delete(item.id);
    this.ds.Info("删除成功！");
    this.GetData();
  }
}
