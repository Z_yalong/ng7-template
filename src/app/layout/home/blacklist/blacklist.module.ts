import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { Routes, RouterModule } from "@angular/router";
import { BlacklistComponent } from "./blacklist.component";
import { BlackItemComponent } from "../../../dialog/home/black-item/black-item.component";
import { DialogXWService } from "../../../service/dialog.service";
import { DialogBasic } from "../../../dialog/basic/dialogbasic";
import { DataTableModule } from "primeng/components/datatable/datatable";

const routes: Routes = [
  {
    path: '',
    component: BlacklistComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    DialogBasic
  ],
  declarations: [
    BlacklistComponent,
    BlackItemComponent
  ],
  entryComponents:[
    BlackItemComponent
  ],
  providers:[
  ]
})
export class BlacklistModule { }
