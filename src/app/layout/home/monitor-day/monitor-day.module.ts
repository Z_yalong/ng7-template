import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { MonitorDayComponent } from "./monitor-day.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DialogBasic } from "../../../dialog/basic/dialogbasic";
import { ShareMonitorModule } from "../../common/sharemonitor.module";
import { DataTableModule } from "primeng/components/datatable/datatable";
import { CalendarModule } from "primeng/components/calendar/calendar";

const routes: Routes = [
  {
    path: '',
    component: MonitorDayComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    DialogBasic,
    DataTableModule,
    CalendarModule,
    ShareMonitorModule
  ],
  declarations: [
    MonitorDayComponent
  ],
  entryComponents:[
  ],
  providers:[
    
  ]
})
export class MonitorDayModule { }
