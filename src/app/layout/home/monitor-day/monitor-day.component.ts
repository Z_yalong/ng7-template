import { Component, OnInit } from '@angular/core';
import { AppconfigService } from "../../../service/appconfig.service";
import { ApiService } from "../../../../environments/environment";
import { DialogXWService } from "../../../service/dialog.service";
import { fadeIn } from "../../../animations/fadein";
import { MonitorResumeComponent } from "../../../dialog/home/monitor-resume/monitor-resume.component";
import { MonitorFaceComponent } from "../../../dialog/home/monitor-face/monitor-face.component";
import { MonitorMobileComponent } from "../../../dialog/home/monitor-mobile/monitor-mobile.component";
import { MonitorEmailComponent } from "../../../dialog/home/monitor-email/monitor-email.component";
import { MatDialogConfig } from "@angular/material/dialog";

@Component({
  selector: 'app-monitor-day',
  templateUrl: './monitor-day.component.html',
  styleUrls: ['./monitor-day.component.css'],
  animations: [fadeIn]
})
export class MonitorDayComponent implements OnInit {
  option: any = {depId:''};
  SelectTime: Date;
  MaxTime: Date;

  functionEmpNamelist=[];
  saleEmpNamelist=[];
  functionStandard:any={};
  saleStandard:any={};

  items: any;
  totalRecords: number;

  groups=[];
 

  show1=false;
  show2=false;

  first=0;//第几页
  authority:any={};//权限

  width150={width:'120px'};
  width100={width:'100px'};
  selectedItem:any;
  
  constructor(private api: ApiService,
    private ds:DialogXWService,
    public config: AppconfigService) { 
    config.getpermision.then(auth=>{
      this.authority = auth['HRMS_02_01'];
    }) }

  ngOnInit() {
    const startDate = new Date();
    this.SelectTime = startDate;
    this.MaxTime=startDate;
    this.option.SelectTime = startDate.Format('yyyy/MM/dd');
    this.api.Monitor.GetRecuitGroup().then(list=>{
      this.groups=list;
    });
  }

  loadItemsLazy(event: any) {
    this.option.pageSize = event.rows;
    this.option.pageIndex = event.first / event.rows + 1;
    console.log(this.option);
    this.GetData();
  }

  Search() {
    this.option.SelectTime = this.SelectTime.Format('yyyy/MM/dd');
    this.option.EndTime = this.option.SelectTime;
    if (this.first)
      this.first = 0;
    else
      this.GetData();
  }
  Export()
  {
    this.option.SelectTime = this.SelectTime.Format('yyyy/MM/dd');
    window.open('/api/Monitor/MonitorExportExcel?SelectTime='+this.option.SelectTime);
  }

  GetData(): void {
    this.api.Monitor.GetStandardMonitorList(this.option).then(result => {
      console.log(result);
      this.items = result.pageList.list;
      this.totalRecords = result.pageList.totalCount;

      this.functionEmpNamelist=result.functionEmpNamelist;
      this.saleEmpNamelist=result.saleEmpNamelist;
      this.functionStandard=result.functionStandard;
      this.saleStandard=result.saleStandard;

      const option = this.option;
    });
  }

  genQueryData(item, title) {
    return {
      // "title": title,
      depId: this.option.group,
      empName: this.option.empName,
      empId: item.empId,
      startTime:  this.option.SelectTime,
      endTime: this.option.SelectTime,
      pageSize: 10,
      pageIndex: 1
    };
  }

  ViewResume(item) {
    this.ds.open(MonitorResumeComponent,<MatDialogConfig> {
      disableClose: true,
      data:this.genQueryData(item, "") ,
      width: '800px'
    });

  }

  ViewFace(item) {
    this.ds.open(MonitorFaceComponent, <MatDialogConfig>{
      disableClose: true,
      data:this.genQueryData(item, "") ,
      width: '800px'
    });
  }

  ViewMobile(item) {
    this.ds.open(MonitorMobileComponent, <MatDialogConfig>{
      disableClose: true,
      data:this.genQueryData(item, "") ,
      width: '800px'
    });
  }

  ViewEmail(item) {
    this.ds.open(MonitorEmailComponent, <MatDialogConfig>{
      disableClose: true,
      data:this.genQueryData(item, "") ,
      width: '800px'
    });
  }
}
