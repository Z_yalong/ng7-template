import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { ResumeComponent } from "./resume.component";

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SourcePipe } from "../../../pipe/source.pipe";
import { ResumemodifyComponent } from "../../../dialog/home/resumemodify/resumemodify.component";
import { PhonecallComponent } from "../../../dialog/home/phonecall/phonecall.component";

import { DialogBasic } from "../../../dialog/basic/dialogbasic";
import { CalendarModule } from "primeng/components/calendar/calendar";
import { DataTableModule } from "primeng/components/datatable/datatable";
import {DropdownModule} from 'primeng/primeng';

const routes: Routes = [
  {
    path: '',
    component: ResumeComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    DialogBasic,
    CalendarModule,
    DropdownModule,
    DataTableModule
  ],
  declarations: [
    ResumeComponent,
    ResumemodifyComponent,
    PhonecallComponent,
    SourcePipe
  ],
  exports:[
    
  ],
  entryComponents:[
    ResumemodifyComponent,
    PhonecallComponent
  ],
  providers:[
    
  ]
})
export class ResumeModule { }
