import { Component, OnInit } from '@angular/core';
import { ResumemodifyComponent } from "../../../dialog/home/resumemodify/resumemodify.component";
import { DatePipe } from '@angular/common';
import { DialogXWService } from "../../../service/dialog.service";
import { PhonecallComponent } from "../../../dialog/home/phonecall/phonecall.component";
import { ApiService } from "../../../../environments/environment";
import { PositionService } from "../../../service/position.service";
import { DomHelperService } from "../../../service/dom-helper.service";
import { AppconfigService } from "../../../service/appconfig.service";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.css']
})
export class ResumeComponent implements OnInit {
  option: any = { Source: '', pageSize: 10}; // 查询参数
  StartTime: Date;
  EndTime: Date;
  items: any; // 查询结果
  totalCount: number; // 总数
  positions= []; // 岗位列表

  first=0;//第几页

  authority: any= {}; // 权限

  width240={width:'210px'};

  postionlist;
  constructor(private api: ApiService,public dialog: MatDialog,private ds: DialogXWService,
    public com: DomHelperService,private thisroute: ActivatedRoute,
    public config: AppconfigService, public postionservice: PositionService ) {
      config.getpermision.then(auth => {
        this.authority = auth['HRMS_01_01'];
      });
      postionservice.positionList.then(list=>{
        this.postionlist=list;
      })
      console.log(thisroute.data["value"])
     }

  ngOnInit() {
  }


  loadItemsLazy(event: any) {
    //in a real application, make a remote request to load data using state metadata from event
    //event.first = First row offset
    //event.rows = Number of rows per page
    //event.sortField = Field name to sort with
    //event.sortOrder = Sort order as number, 1 for asc and -1 for dec
    //filters: FilterMetadata object having field as key and filter value, filter matchMode as value
    console.log(this.first);

    this.option.pageSize = event.rows;
    this.option.pageIndex = this.first / event.rows + 1;
    this.GetData();
      //imitate db connection over a network
      // console.log(event);
    // setTimeout(() => {
    //     if(this.datasource) {
    //         this.cars = this.datasource.slice(event.first, (event.first + event.rows));
    //     }
    // }, 250);
  }

  async Add() {
    const success = await this.ds.open(ResumemodifyComponent, <MatDialogConfig>{
      data: { title: '新增' },
      disableClose: true,
      width: '550px'
    });
    if(success){
      await this.ds.Info("添加成功！");
      this.GetData();
    } 
  }

  async Modify(item) {
    let result = await this.api.Resumes.GetOne(item.id);

    const success = await this.ds.open(ResumemodifyComponent, <MatDialogConfig>{
      data: { title: '修改', id: item.id, data: { name: result.name, mobile: result.mobile, eMail: result.eMail, postId: result.postId } },
      disableClose: true,
      width: '550px'
    });
    if(success){
      await this.ds.Info("修改成功！");
      this.GetData();
    } 
  }

  Call(item) {
    this.dialog.open(PhonecallComponent, <MatDialogConfig>{
      data: { callId: 1, userName: item.name, mobile: item.mobile, resumeId: item.id },
      disableClose: true,
      width: '500px'
    });
  }

  GetData() {
    if ( this.option.Name) {
      this.option.Name = this.option.Name.trim();
    }
    if (this.option.Mobile ) {
      this.option.Mobile = this.option.Mobile.trim();
    }

    this.api.Resumes.GetList(this.option).then(result => {
      this.items = result.list;
      this.totalCount = result.totalCount;
    });
  }

  Serach() {
    var datePipe = new DatePipe("en-US");
    if (this.StartTime)
      this.option.StartTime = datePipe.transform(this.StartTime, 'yyyy/MM/dd');
    else
      this.option.StartTime = null;
    if (this.EndTime)
      this.option.EndTime = datePipe.transform(this.EndTime, 'yyyy/MM/dd');
    else
      this.option.EndTime = null;

    if (this.first)
      this.first = 0;
    else
      this.GetData();
    // new Date().toDateString()
    // console.log(DatePipe. this.option.StartTime.toDateString())
  }
  selectedItem() { }
}
