import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from "@angular/router";
import { MonitorComponent } from "./monitor.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DialogBasic } from "../../../dialog/basic/dialogbasic";
import { ShareMonitorModule } from "../../common/sharemonitor.module";
import { DataTableModule } from "primeng/components/datatable/datatable";
import { CalendarModule } from "primeng/components/calendar/calendar";


const routes: Routes = [
  {
    path: '',
    component: MonitorComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    DialogBasic,
    ReactiveFormsModule,
    DataTableModule,
    CalendarModule,
    ShareMonitorModule
  ],
  declarations: [
    MonitorComponent,
  ],
  entryComponents:[
  ],
  providers:[
    
  ]
})
export class MonitorModule { }
