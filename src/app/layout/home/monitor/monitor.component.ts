import { MonitorEmailComponent } from './../../../dialog/home/monitor-email/monitor-email.component';
import { MonitorMobileComponent } from './../../../dialog/home/monitor-mobile/monitor-mobile.component';
import { DatePipe } from '@angular/common';
import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { MonitorResumeComponent } from '../../../dialog/home/monitor-resume/monitor-resume.component';
import { MonitorFaceComponent } from '../../../dialog/home/monitor-face/monitor-face.component';
import { ApiService } from "../../../../environments/environment";
import { DialogXWService } from "../../../service/dialog.service";
import { AppconfigService } from "../../../service/appconfig.service";
import { LazyLoadEvent } from "primeng/components/common/lazyloadevent";
import { MatDialogConfig } from "@angular/material/dialog";

@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.css']
})
export class MonitorComponent implements OnInit {
  option: any = {depId:''};

  StartTime: Date;
  EndTime: Date;

  items: any;
  totalRecords: number;

  groups=[];

  first=0;//第几页
  authority:any={};//权限

  width150={width:'120px'};
  width100={width:'100px'};
  constructor(private api: ApiService,
    private ds:DialogXWService,
    public config: AppconfigService) { 
      config.getpermision.then(auth=>{
        this.authority = auth['HRMS_02_01'];
      })
     }

  ngOnInit() {
    const startDate = new Date();
    this.StartTime = startDate;
    this.option.StartTime = startDate.Format('yyyy/MM/dd');
    this.EndTime = startDate;
    this.option.EndTime=this.option.StartTime;
    this.api.Monitor.GetRecuitGroup().then(list=>{
      this.groups=list;
    })
  }


  loadItemsLazy(event: LazyLoadEvent) {
    this.option.pageSize = event.rows;
    this.option.pageIndex = event.first / event.rows + 1;
    console.log(this.option);
    this.GetData();
  }

  GetData(): void {
    this.api.Monitor.GetMonitorList(this.option).then(result => {
      this.items = result.list;
      this.totalRecords = result.totalCount;
      const option = this.option;
      this.items.forEach(item => {
        if (option.StartTime === option.EndTime)
          item.date = option.EndTime;
        else
          item.date = option.StartTime + '~' + option.EndTime;
      })
      console.log(result);
    });
  }

  Search() {

    if(!this.StartTime||!this.EndTime){
      this.ds.Alert("开始结束时间必须小于90天！");
      return;
    }

    if(this.EndTime.Minus(this.StartTime)>90){
      this.ds.Alert("开始结束时间必须小于90天！");
      return;
    }
    this.option.StartTime = this.StartTime.Format('yyyy/MM/dd');

    this.option.EndTime =this.EndTime.Format('yyyy/MM/dd');

    if (this.first)
      this.first = 0;
    else
      this.GetData();
    //console.log(this.option);
  }

  selectedItem:any;


  genQueryData(item, title) {
    return {
      // "title": title,
      depId: this.option.group,
      empName: this.option.empName,
      empId: item.empId,
      startTime:  this.option.StartTime,
      endTime: this.option.EndTime,
      pageSize: 10,
      pageIndex: 1
    };
  }

  ViewResume(item) {
    this.ds.open(MonitorResumeComponent, <MatDialogConfig>{
      disableClose: true,
      data:this.genQueryData(item, "") ,
      width: '800px'
    });

  }

  ViewFace(item) {
    this.ds.open(MonitorFaceComponent, <MatDialogConfig>{
      disableClose: true,
      data:this.genQueryData(item, "") ,
      width: '800px'
    });
  }

  ViewMobile(item) {
    this.ds.open(MonitorMobileComponent, <MatDialogConfig>{
      disableClose: true,
      data:this.genQueryData(item, "") ,
      width: '800px'
    });
  }

  ViewEmail(item) {
    this.ds.open(MonitorEmailComponent, <MatDialogConfig>{
      disableClose: true,
      data:this.genQueryData(item, "") ,
      width: '800px'
    });
  }
}
