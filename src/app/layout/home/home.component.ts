import { Component, OnInit } from '@angular/core';
import {treemenunode} from '../../class/treemenunode';
import {ActivatedRoute, Router} from '@angular/router';
import {TabService} from '../../service/tabservice.service';
import {MyRouterReuseStrategy} from '../../service/router.snapshot';
import {contentantimate} from '../../animations/fadein';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less'],
  animations: [contentantimate]
})
export class HomeComponent implements OnInit {

  initmenu: Promise<void>;//初始化菜单
  public shownavsate = 'show'; // 左边菜单展示状态
  public treeData: treemenunode[] = []; // 左边菜单数据
  constructor(private route: Router, private thisroute: ActivatedRoute, public tabservice :TabService) { // 导航菜单数据初始化
  }

  ngOnInit() {
    this.initmenu=this.tabservice.getmenu.then(list=>{
      this.treeData = list;
    })
  }

  // 菜单点击事件 clear:是否删除快照
  menuclick(node: treemenunode,clear:boolean) {
    if(clear)
      MyRouterReuseStrategy.Remove(node.routername);
    this.route.navigate([node.routername], {relativeTo: this.thisroute});
  }

  // @ViewChildren(DynamicComponent) tabcoms: QueryList<DynamicComponent>;

  // selectindex: number = 0;//当前活动tab窗口的index
  // tabs = [];//tab列表

  //private tabobj = {};//tab集合查询管理对象

  // //打开新页面
  // OpenTab(title: string, type: any, params?: any) {
  //   let id: string = title;
  //   if (params)
  //     Object.keys(params).forEach(key => {
  //       id += params[key];
  //     })
  //   if (this.tabobj[id]) {//当前tab已存在
  //     this.selectindex = this.tabs.indexOf(this.tabobj[id]);
  //     let result = this.FindComponent(type);
  //     console.log(result);
  //     return;
  //   }
  //   let tab = {
  //     id:id,
  //     title: title,
  //     componentdata: {
  //       component: type,
  //       params: params
  //     }
  //   };
  //   this.tabobj[id] = tab;
  //   this.tabs.push(tab);
  //   this.selectindex = this.tabs.length - 1;
  // }

  //关闭页面
  CloseTab(index: number, event: MouseEvent) {
    MyRouterReuseStrategy.Remove(this.tabservice.tabs[index].routername);//删除快照

    this.tabservice.tabs[index].showclose=false;
    this.tabservice.tabs.splice(index, 1);
    if(this.tabservice.selectindex==index){
      this.tabservice.selectindex--;
      this.route.navigate([this.tabservice.tabs[this.tabservice.selectindex].routername], {relativeTo: this.thisroute});
    }
    else{
      if(index<this.tabservice.selectindex) this.tabservice.selectindex--;
    }
    event.preventDefault();
    event.stopPropagation();
  }

  // //菜单点击事件
  // menuclick(node: treemenunode) {
  //   this.OpenTab(node.title, node.type, { });
  //   // console.log(this.tabcoms);
  //   //let result=this.FindComponent(SampleMainComponent);//查找制定tab页面组件
  // }

  // //查找制定tab页面组件
  // FindComponent<ComponentE>(tp: ComponentE): ComponentE {
  //   let findresult = null;
  //   let tcp:any = tp;
  //   this.tabcoms.forEach(t => {
  //     if (t.currentComponent.instance instanceof tcp) {
  //       findresult = t.currentComponent.instance;
  //     }
  //   });
  //   return findresult;
  // }



}
