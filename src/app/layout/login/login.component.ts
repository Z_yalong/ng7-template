import {Component, HostListener, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CacheService} from '../../service/cache.service';
import {HttpService} from '../../service/http.service';
import {ApiService} from '../../../environments/environment';
import {AppconfigService} from '../../service/appconfig.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  imgsrc: string;
  islogin = false;

  constructor(private formBuilder: FormBuilder,
              private api: ApiService,
              private route: Router,
              private usercache: CacheService,
              private config: AppconfigService,
              private http: HttpService
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userName: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required, Validators.minLength(4)]],
      validateCode: ['', [Validators.required, Validators.minLength(4)]],
      isPersistent: [false]
    });
    if (this.usercache.LastUserName) {
      this.loginForm.controls['userName'].setValue(this.usercache.LastUserName);
      this.loginForm.controls['password'].setValue(this.usercache.UserCache[this.usercache.LastUserName]);
      this.loginForm.controls['isPersistent'].setValue(true);
    }
    this.loginForm.controls['userName'].valueChanges.subscribe((username: string) => {
      if (this.usercache.UserCache[username]) {
        this.loginForm.controls['password'].setValue(this.usercache.UserCache[username]);
        this.loginForm.controls['isPersistent'].setValue(true);
      } else {
        this.loginForm.controls['password'].setValue('');
        this.loginForm.controls['isPersistent'].setValue(false);
      }
    });
  }

  ChangeImg() {
    // 切换验证码
    // this.imgsrc=this.api.Account.VerifyImage+"?"+new Date().getTime();
  }


  login() {
    if (this.loginForm.invalid || this.islogin) {
      return;
    }

    this.islogin = true;
    // this.api.Account.Login(this.loginForm.value).then(() => {
    //   this.usercache.SetUser(this.loginForm.value, this.loginForm.value.isPersistent);
    //   this.config.Flush();
      this.route.navigate(["home"]);
    // }).catch(() => {
    //   this.islogin = false;
    //   this.ChangeImg();
    // })
  }

  @HostListener('document:keypress', ['$event'])
  keypress(e: KeyboardEvent) {
    if (e.keyCode === 13) {
      this.login();
    }
  }
}
