import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrueFalsePipe } from "../../pipe/truefalse.pipe";
import { MonitorResumeComponent } from "../../dialog/home/monitor-resume/monitor-resume.component";
import { MonitorFaceComponent } from "../../dialog/home/monitor-face/monitor-face.component";
import { MonitorMobileComponent } from "../../dialog/home/monitor-mobile/monitor-mobile.component";
import { MonitorEmailComponent } from "../../dialog/home/monitor-email/monitor-email.component";
import { DialogBasic } from "../../dialog/basic/dialogbasic";
import { DataTableModule } from "primeng/components/datatable/datatable";

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    DialogBasic
  ],
  declarations: [
    TrueFalsePipe,
    MonitorResumeComponent,
    MonitorFaceComponent,
    MonitorMobileComponent,
    MonitorEmailComponent
  ],
  exports:[
    // TrueFalsePipe,
    // MonitorResumeComponent,
    // MonitorFaceComponent,
    // MonitorMobileComponent,
    // MonitorEmailComponent
  ],
  entryComponents:[
    MonitorResumeComponent,
    MonitorFaceComponent,
    MonitorMobileComponent,
    MonitorEmailComponent
  ]
})
export class ShareMonitorModule {

 }
