import { Component, OnInit, AfterViewInit, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { treemenunode } from "../../../class/treemenunode";
import { fadeIn } from "../../../animations/fadein";

@Component({
  selector: 'app-navtreeitem',
  templateUrl: './navtreeitem.component.html',
  styleUrls: ['./navtreeitem.component.css'],
  animations: [fadeIn]
})
export class NavtreeitemComponent implements OnInit, AfterViewInit {
  @Input() level: number;
  @Input() item: treemenunode;
  @ViewChild('children') children: ElementRef;
  @Output() nodeclick: EventEmitter<any> = new EventEmitter();
  @Output() closeothernode: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
   // if (this.item.children && !this.item.isExpend) $(this.children.nativeElement).hide();
  }

  //节点单击
  childnodeclick(node: treemenunode) {
    if (!node.children) {
      this.nodeclick.emit(node);//告诉父级节点处理跳转
    }
    else {
      node.isExpend = !node.isExpend;
      if (node.children && node.children.length) {
        if (node.isExpend) {
         // $(this.children.nativeElement).slideDown("fast");//展开
          this.closeothernode.emit(this);//告诉父节点关闭兄弟节点菜单
        }
       // else $(this.children.nativeElement).slideUp("fast");//收缩
      }
    }
  }

  CloseExpend() {
    if (this.item.isExpend) {
      this.item.isExpend = false;
     // $(this.children.nativeElement).slideUp("fast");
    }
  }

}
