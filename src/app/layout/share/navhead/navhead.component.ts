import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ViewChildren, QueryList, Inject, forwardRef } from '@angular/core';
import { Router } from "@angular/router";
import {HomeComponent} from '../../home/home.component';
import {AppconfigService} from '../../../service/appconfig.service';
import {fadeIn} from '../../../animations/fadein';
import {MyRouterReuseStrategy} from '../../../service/router.snapshot';
import {treemenunode} from '../../../class/treemenunode';

@Component({
  selector: 'app-navhead',
  templateUrl: './navhead.component.html',
  styleUrls: ['./navhead.component.less'],
  animations: [fadeIn]
})
export class NavheadComponent implements OnInit {
  shownavsateValue: string;
  @Input()
  get shownavsate(){
    return this.shownavsateValue;
  }
  set shownavsate(value){
    this.shownavsateValue=value;
    this.shownavsateChange.emit(value)
  }
  @Output() shownavsateChange: EventEmitter<string> = new EventEmitter();
  // private setactive = false;
  // @ViewChild('op') op: OverlayPanel;
  public showdropdown = false;

  constructor(private route: Router,
    public config:AppconfigService ,
    @Inject(forwardRef(() => HomeComponent)) public home: HomeComponent)
  { }

  ngOnInit() {
    this.home.initmenu.then(()=>{
      this.Copy(this.home.treeData,this.menuitems);
      //console.log(this.menuitems);
    });
  }

  Exit(){
    MyRouterReuseStrategy.Remove("home");
    this.route.navigate(['login']);
  }

  Copy(orginarr: treemenunode[], newarr: any[]) {
    orginarr.forEach(item => {
      let additem:any = {
        label: item.name,
        icon: item.iconclass
      }
      if(item.children&&item.children.length){
        additem.items=[];
        this.Copy(item.children,additem.items);
      }
      else{
        // additem.command=(event:MouseEvent)=>{
        //   console.log(this);
        // };
        additem.routerLinkActiveOptions=item;
        additem.command=this.menunodeclick;
      }
      newarr.push(additem);
    })
  }

  navslide() {
    if (this.shownavsate == 'show') {
      this.shownavsate = 'hide';
      this.showdropdown = true;
    }
    else {
      this.shownavsate = 'show';
      this.showdropdown = false;
    }

    //this.shownavsate = !this.shownavsate;
    //this.navmenuService.navchange.emit(this.shownavsate);
  }

  // setclick(event: MouseEvent) {
  //   this.setactive = !this.setactive;
  //   this.op.toggle(event);
  // }

  menuitems: any[]=[];
  // @ViewChildren(TieredMenu)
  // menus: QueryList<TieredMenu>;
  // menuclick(i:number,e:MouseEvent){
  //   let menu= this.menus.toArray()[i];
  //   menu.toggle(e);
  //   //console.log(this.menus);
  // }

  menunodeclick=(e:{orginalEvent :MouseEvent,item:any})=>{
    this.home.menuclick(e.item.routerLinkActiveOptions,true);
  };

}
