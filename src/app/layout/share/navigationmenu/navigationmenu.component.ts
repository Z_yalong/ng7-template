import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { navantimate } from "../../../animations/fadein";
import { treemenunode } from "../../../class/treemenunode";

@Component({
  selector: 'app-navigationmenu',
  templateUrl: './navigationmenu.component.html',
  styleUrls: ['./navigationmenu.component.css'],
  animations: [navantimate]
})
export class NavigationmenuComponent implements OnInit {
  private activenode: treemenunode;
  @Input() treeData: treemenunode[] = [];
  @Input() shownavsate: string='show';
  @Output() menuclick: EventEmitter<any> = new EventEmitter();

  constructor() {
    
  }

  ngOnInit() {
  }
  //菜单点击事件
  nodeclick(node: treemenunode) {
    if (this.activenode)
      this.activenode.active = false;
    node.active = true;
    this.activenode = node;
    this.menuclick.emit(node);//告诉父级节点处理跳转
    //console.log(node);
  }
}
