import { Component, OnInit, AfterViewInit, Input, ViewChildren, EventEmitter, Output, QueryList } from '@angular/core';
import { NavtreeitemComponent } from "../navtreeitem/navtreeitem.component";
import { treemenunode } from "../../../class/treemenunode";

@Component({
  selector: 'app-navtree',
  templateUrl: './navtree.component.html',
  styleUrls: ['./navtree.component.css']
})
export class NavtreeComponent implements OnInit, AfterViewInit {
  @Input() level: number=-1;
  @ViewChildren(NavtreeitemComponent) treeitems: QueryList<NavtreeitemComponent>;
  @Input() treeData: treemenunode[];
  @Output() nodeclick: EventEmitter<any> = new EventEmitter();

  expendnode: treemenunode;

  constructor() {
  
  }

  ngOnInit() {
    this.level++;
  }

  ngAfterViewInit() {
  //  console.log(this.treeitems);

  }

  //节点单击
  childnodeclick(node: treemenunode) {
    if (!node.children)
      this.nodeclick.emit(node);//告诉父级节点处理跳转
    else {
      node.isExpend = !node.isExpend;
      if (this.expendnode) {
        this.expendnode.isExpend = false;
        this.expendnode = null;
      }
      if (node.isExpend) this.expendnode = node;
    }
  }

  //关闭同等级节点
  closeothernode(treeitem: NavtreeitemComponent) {
    this.treeitems.forEach((item) => {
      if (item != treeitem) {
        item.CloseExpend();
      }
    })
  }
}
