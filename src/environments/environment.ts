import {Injectable} from '@angular/core';
import {HttpService} from '../app/service/http.service';
import {RequestOptionsArgs} from '@angular/http';

export const environment = {
  production: false
};

@Injectable()
export class ApiService {
  constructor(private http: HttpService) {
  }

  GetPermissions = () => this.http.get('/assets/json/permision.json');

  GetEntryAreaList = () => this.http.get('/assets/json/entryarea.json');

  Info = {
    GetEmployeeList: (id: number, option?: RequestOptionsArgs) => this.http.get('/assets/json/info.getEmployeeList.json'),
    GetPositionList: () => this.http.get('/assets/json/position.json'),
    GetDepartmentList: () => this.http.get('/assets/json/department.json')
  };

  //resume
  Resumes = {
    GetList: (option: any) => this.http.get('/assets/json/resume.json'),
    GetOne: (id: number) => this.http.get('/assets/json/resumeone.json'),
    Add: (option: any) => this.http.get('/assets/json/success.json'),
    Edit: (option: any) => this.http.get('/assets/json/success.json')
  };

  //blacklist
  Black = {
    GetList: (option: any) => this.http.get('/assets/json/blacklist.json')
    , Delete: (id: number) => this.http.get('/assets/json/success.json')
    , GetOne: (id: number) => this.http.get('/assets/json/blackmodify.json')
    , Add: (option: any) => this.http.get('/assets/json/success.json')
    , Edit: (option: any) => this.http.get('/assets/json/success.json')
    , ImportExcel: (option: any) => this.http.get('/assets/json/success.json')
  };

  //monitor
  Monitor = {
    GetMonitorList: (option: any) => this.http.get('/assets/json/monitor.json'),
    GetSendMailList: (option: any) => this.http.get('/assets/json/monitor.mail.json'),
    GetInterviewList: (option: any) => this.http.get('/assets/json/monitor.face.json'),
    GetResumeList: (option: any) => this.http.get('/assets/json/monitor.resume.json'),
    GetMobileList: (option: any) => this.http.get('/assets/json/monitor.phone.json'),
    GetRecuitGroup: () => this.http.get('/assets/json/monitor.group.json'),
    GetStandardMonitorList: (option: any) => this.http.get('/assets/json/monitor_day.json')
  };

  //recruit
  Recruit = {
    GetList: (option: any) => this.http.get('/assets/json/recruit.list.json'),
    Delte: (delids: number[]) => this.http.get('/assets/json/success.json'),
    Add: (option: any) => this.http.get('/assets/json/success.json')
  };

  //entry
  EntryPower = {
    GetUsers: (option: any) => this.http.get('/assets/json/entrypower.getusers.json'),
    DeleteUsers: (id: number) => this.http.get('/assets/json/success.json'),
    AddUsers: (option: any) => this.http.get('/assets/json/success.json')
  };
}
